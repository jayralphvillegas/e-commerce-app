import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Header from '../admin/Header';
import { Link } from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';

export default function UserDetails() {
  const [users, setUsers] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => setUsers(data))
      .catch(err => console.log(err));
  }, []);

  const handlePromoteAdmin = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      },
      body: JSON.stringify({ isAdmin: true })
    })
      .then(res => res.json())
      .then(data => {
        // Update the users state with the updated user data
        setUsers(users.map(user => {
          if (user._id === data._id) {
            return data;
          }
          return user;
        }));
      })
      .catch(err => console.log(err));
  };

  return (
    <Container>
      <Header />
      <Row className="mt-5 align-items-center  justify-content-center">
        <Col className='bg-light rounded-3'>
          {users ?
            <div className="card">
              <h5 className="card-header bg-success text-center">All Details</h5>
              <ul className="list-group list-group-flush">
                {users.map(user => (
                  <li className="list-group-item" key={user._id}>
                    User ID: {user._id}<br />
                    Full Name: {user.firstName} {user.lastName}
                    {user.isAdmin ?
                      <span className="badge bg-primary rounded-pill ms-2">Admin</span>
                      :
                      <Button variant="success" className="ms-2" onClick={() => handlePromoteAdmin(user._id)}>Promote to admin</Button>
                    }
                    <Link className="btn float-end" to={`/admin/updateUser`}><FaEdit size='1.5rem' /></Link><br />
                    Email: {user.email}<br />
                    Mobile No: {user.mobileNo}
                  </li>
                ))}

              </ul>
            </div>
            :
            <p>Loading user details...</p>
          }
        </Col>
      </Row>
    </Container>
  );
}
