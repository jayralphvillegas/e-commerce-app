import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Header from '../admin/Header';

export default function UserDetails() {
  const [user, setUser] = useState(null);
  const [orders, setOrders] = useState(null);

  useEffect(() => {
    // Retrieve user details
    fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => setUser(data))
      .catch(err => console.log(err));

    // Retrieve all orders
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => setOrders(data))
      .catch(err => console.log(err));
  }, []);

  return (
    <Container>
      <Header />
      <Row className="mt-5 align-items-center justify-content-center">
        <Col className=' rounded-3'>

          {orders && (
            <div className="card">
              <h5 className="card-header bg-success text-center">Order History</h5>
              <ul className="list-group list-group-flush">
                {orders.map(order => (
                  <li className="list-group-item" key={order._id}>
                    Order ID: {order._id}<br />
                    Total Amount: {order.totalAmount}<br />
                    Purchased On: {order.purchasedOn}<br />
                  </li>
                ))}
              </ul>
            </div>
          )}

          {!user && !orders && (
            <p>Loading user details and order history...</p>
          )}
        </Col>
      </Row>
    </Container>
  );
}
