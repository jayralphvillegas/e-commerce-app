import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Header from '../admin/Header';


export default function UserDetails() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    })
      .then(res => res.json())
      .then(data => setUser(data))
      .catch(err => console.log(err));
  }, []);

  

  return (
    <Container>
        <Header />
        <Row  className="mt-5 align-items-center  justify-content-center">
            <Col className='bg-light rounded-3'>
                {user ?
                    <div className="card">
                        <h5 className="card-header bg-success text-center">User Details</h5>
                        <ul className="list-group list-group-flush">
                        <li className="list-group-item">User ID: {user._id}</li>
                        <li className="list-group-item">Full Name: {user.firstName} {user.lastName}</li>
                        <li className="list-group-item">Email: {user.email}</li>
                        <li className="list-group-item">Mobile No: {user.mobileNo}</li>
                        </ul>
                    </div>
                    :
                    <p>Loading user details...</p>
                }
            </Col>
        </Row>
    </Container>

    
  

    );
}
