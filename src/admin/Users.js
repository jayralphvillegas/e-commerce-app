import { useEffect, useState } from 'react';
import UserCard from '../admin/AdminProductCard';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import Header from '../admin/Header';

export default function AllUsers(){

	const [ user, setUsers ] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/all`)
		.then(res => res.json())
		.then(data => {

			setUsers(data.map(user => {

				return (
					<UserCard key={ user._id } user={user} />
					
				);
			}));
		})
	}, []);

	return (
		<Container>
			<Header />
			<Row>
				<Col>
				{ user }
				</Col>
			</Row>
		</Container>
		
	)
}

