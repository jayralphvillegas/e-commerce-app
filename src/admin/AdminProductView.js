// ============working ========================
import { useState, useEffect, useContext } from 'react';
import { Container, Table, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import Header from '../admin/Header';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function UpdateProduct() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setIsActive(data.isActive);
      })
      .catch((error) => {
        console.error(error);
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.',
        });
      });
  }, [productId]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      name,
      description,
      price,
      isActive,
    };
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        Swal.fire({
          title: 'Successful',
          icon: 'success',
          text: 'Product details updated successfully!',
        });
        navigate('/admin/allProducts');
      })
      .catch((error) => {
        console.error(error);
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.',
        });
      });
  };

  return (
    <Container>
      <Header />
      <Row className='pt-5 mt-5 d-flex flex-wrap justify-content-center align-items-center'>
        <Col md={6} sm={12}>
          <Table striped bordered hover>
          <thead>
            <tr>
              <th>Field</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Name</td>
              <td>
                <input className='w-100' type="text" value={name} onChange={(e) => setName(e.target.value)} />
              </td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <textarea className='w-100' rows={3} value={description} onChange={(e) => setDescription(e.target.value)} />
              </td>
            </tr>
            <tr>
              <td>Price</td>
              <td>
                <input className='w-100' type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
              </td>
            </tr>
            <tr>
              <td>{isActive ? 'Active' : 'Archive'  }</td>
              <td>
                <input type="checkbox" checked={isActive} onChange={(e) => setIsActive(e.target.checked)} />
                <span style={{ marginLeft: '5px' }}>
                  {isActive ?  '   Uncheck the checkbox to archive this product' :  '   Check the checkbox to activate this product'}
                </span>
              </td>
            </tr>
          </tbody>
          </Table>
          <Button className='w-100' variant="primary" type="submit" onClick={handleSubmit}>
            Save
          </Button>
        </Col>
      </Row>
    </Container>
  );
}
