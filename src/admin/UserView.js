import { useState, useEffect } from 'react';
import { Container, Table, Button, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import Header from '../admin/Header';

import Swal from 'sweetalert2';

export default function UserView() {
  const { userId } = useParams();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [isAdmin, setIsAdmin] = useState('');

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setEmail(data.email);
        setMobileNo(data.mobileNo);
        setIsAdmin(data.isAdmin);
      })
      .catch((error) => {
        console.error(error);
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.',
        });
      });
  }, [userId]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {
      firstName,
      lastName,
      email,
      mobileNo,
      isAdmin,
    };
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        Swal.fire({
          title: 'Successful',
          icon: 'success',
          text: 'User details updated successfully!',
        });
      })
      .catch((error) => {
        console.error(error);
        Swal.fire({
          title: 'Something went wrong',
          icon: 'error',
          text: 'Please try again.',
        });
      });
  };

  return (
    <Container>
      <Header />
      <Row className='pt-5 mt-5 d-flex flex-wrap justify-content-center align-items-center'>
        <Col md={6} sm={12}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Field</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>First Name</td>
                <td>
                  <input className='w-100' type="text" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                </td>
              </tr>
              <tr>
                <td>Last Name</td>
                <td>
                  <input className='w-100' type="text" value={lastName} onChange={(e) => setLastName(e.target.value)} />
                </td>
              </tr>
              <tr>
                <td>Email</td>
                <td>
                  <input className='w-100' type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </td>
              </tr>
              <tr>
                <td>Mobile No.</td>
                <td>
                  <input className='w-100' type="number" value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} />
                </td>
              </tr>
              <tr>
                <td>Status</td>
                <td>
                  <input className='w-100' type="text" value={isAdmin} onChange={(e) => setIsAdmin(e.target.value)} />
                </td>
              </tr>
            </tbody>
            
          </Table>
          <Button className='w-100' variant="primary" type="submit" onClick={handleSubmit}>
            Save
          </Button>
        </Col>
      </Row>
    </Container>

  );
}