import { Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FaEdit } from 'react-icons/fa';

export default function UserCard({ user }) {
  const { _id, firstName, lastName, email} = product;

  return (

    <Container>
      <Header />
      <Row  className="mt-5 align-items-center  justify-content-center">
          <Col className='bg-light rounded-3' xs={11} md={7} lg={5}>
              {user ?
                  <div className="card">
                      <div className="card-header bg-success">User Details</div>
                      <ul className="list-group list-group-flush">
                      <li className="list-group-item">User ID: {user._id}</li>
                      <li className="list-group-item">Full Name: {user.firstName} {user.lastName}</li>
                      <li className="list-group-item">Email: {user.email}</li>
                      <li className="list-group-item">Mobile No: {user.mobileNo}</li>
                      </ul>
                  </div>
                  :
                  <p>Loading user details...</p>
              }
          </Col>
      </Row>
    </Container>
      
  );
}
