const productsData = [
    {
        id: "wdc001",
        name: "Biogesic",
        description: "Inumin mo to 3x a day para mawala sakit ng ulo mo.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Bioflu",
        description: "Inumin mo to 3x a day pra mawala lagnat mo.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Neozep",
        description: "Inumin mo to 3x a day para mawala sipon mo.",
        price: 55000,
        onOffer: true
    }
]

export default productsData;