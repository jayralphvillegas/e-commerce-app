// built-in react modules imports
import { useState, useEffect } from 'react';

// downloaded package modules imports
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import './App.css';
// (user-defined) components imports (alphabetical or according file structure)

// =====ADMIN=====
import AddProduct from './admin/AddProduct';
import UpdateProduct from './admin/AdminProductView';
import AllProducts from './admin/AllProducts';
import AdminProductView from './admin/AdminProductView';
import UserView from './admin/UserView';
import Dashboard from './pages/Dashboard';
import UserDetails from './admin/UserDetails';
import AllUsers from './admin/AllUsers';
import AllOrders from './admin/AllOrders';
import Users from './admin/Users';

//  =====COMPONENTS=====
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';



// =====PAGES======
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Checkout from './pages/Checkout';
import UserOrders from './components/UserOrders';
import Products from './pages/Products';

// import AdminDashboard from './pages/Dashboard';


import { UserProvider } from './UserContext'

function App() {

  const [ user, setUser ] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      console.log(data)

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    });
  }, [])

  return (

    <UserProvider value={{ user, setUser, unsetUser }}>

      <Router>      
        <Container fluid>   
          <AppNavbar />     
            <Routes>
              
              <Route path="/admin/allProducts" element={<AllProducts />} />
              <Route path="/admin/allUsers" element={<AllUsers />} />
              <Route path="/admin/addProduct" element={<AddProduct />} />
              <Route path="/admin/users" element={<Users />} />

              <Route path="/admin/updateProduct/:productId" element={<UpdateProduct />} />
              <Route path="/admin/updateProduct" element={<UpdateProduct />} />
              <Route path="/admin/userDetails" element={<UserDetails />} />
              <Route path="/admin/updateProducts/:productId" element={<AdminProductView />} />
              <Route path="/admin/updateUser/:userId" element={<UserView />} />

              <Route path="/products/:productId/checkout" element={<ProductView />} />
              <Route path="/checkout" element={<Checkout />} />


              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />}/>
              <Route path="/admin/dashboard" element={<Dashboard />} />
              <Route path="/register" element={<Register />} />

              <Route path="/admin/allOrders" element={<AllOrders />} />
              <Route path="/user/orders" element={<UserOrders />} />              
              <Route path="*" element={<Error />} />

            </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;
