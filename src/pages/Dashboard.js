import { useEffect, useState } from 'react';
import { Row, Col, Table, Container } from 'react-bootstrap';
import AdminProductCard from '../admin/AdminProductCard';
import Header from '../admin/Header';
import AddProduct from '../admin/AddProduct';
import UpdateProduct from '../admin/UpdateProduct'; // Import UpdateProduct component

export default function Products() {

  const [ products, setProducts ] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/`)
    .then(res => res.json())
    .then(data => setProducts(data))
  }, []);

  return (
    <Container className="productCard justify-content-center mt-5 container-fluid ">

      <Header />
      {/* <AddProduct />
      { products.map(product => (
        <div className='mx-3' key={product._id}>
          <AdminProductCard product={product} />
        </div>
      ))} */}
    </Container>
  )
}




// import { useState, useEffect, useContext } from 'react';
// import { useParams, useNavigate, Link } from 'react-router-dom';
// import { Table, Container, Row, Col } from 'react-bootstrap';

// export default function Dashboard() {
//   return (
//     <Container className="mt-5">
//       <Row className='justify-content-center align-items-center'>
//         <Col>
//             <h3 className='text-center pt-5 justify-content-center align-items-center'>Admin Dashboard</h3>
//             <Table striped bordered hover>
                
//                 <tbody>
//                     <tr>
//                     <td>
//                         <Link className="btn btn-primary btn-block w-100" to="/admin/addProduct">Add New Products</Link>
//                     </td>
//                     <td>
//                         <Link className="btn btn-primary btn-block w-100" to="/admin/allProducts">Show All Products</Link>
//                     </td>
//                     <td>
//                         <Link className="btn btn-primary btn-block w-100" to="/admin/allOrders">Show All Orders</Link>
//                     </td>
//                     <td>
//                         <Link className="btn btn-primary btn-block w-100" to="/admin/allOrders">Show User Orders</Link>
//                     </td>
//                     </tr>
//                 </tbody>
//             </Table>
          
//         </Col>
//       </Row>
//     </Container>


    
//   );
// }
