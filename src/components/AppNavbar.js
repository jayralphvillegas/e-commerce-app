import { useRef, useState, useEffect, useContext } from 'react';
import { FaStore } from 'react-icons/fa';
import { FaCartArrowDown } from 'react-icons/fa';
import { Container, Navbar, Nav } from "react-bootstrap";
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){
  const [isExpanded, setIsExpanded] = useState(false);
  const navRef = useRef(null);
  const { user } = useContext(UserContext);

  useEffect(() => {
    const handleOutsideClick = (event) => {
      if (navRef.current && !navRef.current.contains(event.target)) {
        setIsExpanded(false);
      }
    };

    document.addEventListener('mousedown', handleOutsideClick);

    return () => {
      document.removeEventListener('mousedown', handleOutsideClick);
    };
  }, [navRef]);

  return (
    <Navbar className='bg-success ms-auto' expand="lg" fixed="top" expanded={isExpanded} ref={navRef}>
      <Container fluid >
        <Navbar.Brand as={Link} to="/">
          <FaStore size='2rem' /> <strong>KMT Garage</strong>
        </Navbar.Brand>
		<Nav.Link className='float-end ms-auto mx-3' as={NavLink} to="/products"><FaCartArrowDown size='2rem' />   
            </Nav.Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={() => setIsExpanded(!isExpanded)} />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/">  
              <strong>Home</strong>
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products">
              <strong>Products</strong>
            </Nav.Link>
            {user.id !== null && !user.isAdmin && (
              <Nav.Link as={NavLink} to="/user/orders">
                <strong>Orders</strong>
              </Nav.Link>
            )}
            {user.id !== null && user.isAdmin && (
              <Nav.Link as={NavLink} to="/admin/dashboard">
                <strong>Dashboard</strong>
              </Nav.Link>
            )}
            {user.id !== null && (
              <Nav.Link as={NavLink} to="/logout">
                <strong>Logout</strong>
              </Nav.Link>
            )}
            {user.id === null && (
              <>
                <Nav.Link as={NavLink} to="/login">
                  <strong>Login</strong>
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register">
                  <strong>Register</strong>
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
