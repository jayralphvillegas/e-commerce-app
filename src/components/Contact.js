import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register(){

	const { user } = useContext(UserContext);

	const [ successRegistration, setSuccessRegistration ] = useState();
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				email: email,
			}),
		})
			.then((res) => res.json())
			.then((data) => {

			if(!data){
		
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,
				{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,

					})
				})
				.then(res => res.json())
				.then(data => {

					if(data){

						setSuccessRegistration(data);
						setFirstName('');
						setLastName('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to JRV Drug Store!"
						});
					} 
				}).catch((error) => console.log(error));
				
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Email already in use!"
				});
			}
		})
		.catch((error) => console.log(error));


	}

	useEffect(() => {

		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2]);

	return successRegistration ? ( 

		<Navigate to="/login" /> )
		: 
	(

		<Container className='mt-5 pt-5'>
			<Row  className="align-items-center  justify-content-center">
				<Col className='bg-light rounded-3' xs={11} md={8} lg={5}>
					<h3 className='text-center pt-2'>Register</h3>
					<Form onSubmit={(e) => registerUser(e)}>
						<Form.Group  controlId="userFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control 
							type="text"
							placeholder="Enter first name"
							value={ firstName }
							onChange={e => setFirstName(e.target.value)}
							required/>
						</Form.Group>

						<Form.Group  controlId="userLastName">
							<Form.Label>Last Name</Form.Label>
							<Form.Control 
								type="text" 
								placeholder="Enter last name"
								value={ lastName }
								onChange={e => setLastName(e.target.value)}
								required/>
						</Form.Group>

						<Form.Group  controlId="userMobileNo">
							<Form.Label>Mobile No</Form.Label>
							<Form.Control 
								type="tel" 
								placeholder="Enter mobile no"
								value={ mobileNo }
								onChange={e => setMobileNo(e.target.value)}
								required/>
							<Form.Text className="text-muted">
								Please enter your mobile no with at least 11 digits.
							</Form.Text>
						</Form.Group>

						<Form.Group  controlId="userEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								placeholder="Enter email"
								value={ email }
								onChange={e => setEmail(e.target.value)}
								required/>
							<Form.Text className="text-muted">
							We'll never share your email with anyone else.
							</Form.Text>
						</Form.Group>

						<Form.Group  controlId="password1">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Password"
								value={ password1 }
								onChange={e => setPassword1(e.target.value)} 
								required/>
						</Form.Group>
						
						<Form.Group className='mb-3'  controlId="password2">
							<Form.Label>Verify Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Verify Password"
								value={ password2 }
								onChange={e => setPassword2(e.target.value)}  
								required/>
						</Form.Group>

						{ isActive ?
							<Button className='w-100' variant="success" type="submit" id="submitBtn" block>
							Submit
							</Button>
							:
							<Button className='w-100' variant="success" type="submit" id="submitBtn" block disabled>
							Please enter your registration details
							</Button>
						}
					</Form>
					<h6 className='text-center py-3'>Already have an account? <a href='/login'>Click here</a> to log in.</h6>
				</Col>
			</Row>
		</Container>
	)
}
