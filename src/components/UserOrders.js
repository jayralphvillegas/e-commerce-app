import { useState, useEffect, useContext } from 'react';
import { Container, Card } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function UserOrders() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    if (user.id !== null) {
      fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setOrders(data.reverse()); // Reverse the orders array
        });
    }
  }, [user.id]);

  return (
    <Container className="mt-5">
      <h3 className="text-center mt-5 pt-5">Order History</h3>
      {orders.length === 0 ? (
        <h2>You have no orders yet.</h2>
      ) : (
        orders.map((order, index) => (
          <Card className="my-3" key={index}>
            <Card.Body>
              <Card.Title>Order {index + 1}</Card.Title>
              <Card.Subtitle>Purchased On:</Card.Subtitle>
              <Card.Text>
                {new Date(order.purchasedOn).toLocaleString()}
              </Card.Text>
              <Card.Subtitle>Total Amount:</Card.Subtitle>
              <Card.Text>Php {order.totalAmount}</Card.Text>
              <Card.Subtitle>Products:</Card.Subtitle>
              <ul>
                {order.products.map((product, index) => (
                  <li key={index}>
                    Product ID: {product.productId}, Quantity: {product.quantity}
                  </li>
                ))}
              </ul>
            </Card.Body>
          </Card>
        ))
      )}
    </Container>
  );
}
