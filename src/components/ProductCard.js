import { Card, Container, Row, Col, ButtonGroup, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import givibox from '../images/givibox.webp';

export default function ProductCard({ product }) {
  const { _id, name, description, price } = product;

  return (
    <Container fluid>
      <Card id='productCard' className='rounded-3 bg-light'>
        <Card.Body className='text-center '>
          <Card.Img className='w-75' variant="top" src={givibox} />
          <Card.Title>{name}</Card.Title>
          <Card.Text>{description}</Card.Text>
          <Card.Text>PHP {price}</Card.Text>
          <ButtonGroup className='w-100'>
            <Link className="btn btn-success mx-2 rounded-3 w-50" to={`/products/${_id}/checkout`}>
              Buy Now
            </Link>
            <Link className="btn btn-success mx-2 rounded-3 w-50" to={`/products/${_id}/checkout`}>
              Add to Cart
            </Link>
          </ButtonGroup>
        </Card.Body>
      </Card>
    </Container>
  );
}
